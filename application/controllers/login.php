<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('login_model');
	}

	public function index() {
		$this->load->view('login');
	}

	public function handle_login() {

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$user = $this->login_model->login($username, md5($password));

		if (!empty($user)) {
			$session_data = array(
		       'username' => $username,
		       'logged_in' => TRUE
		    );
		    
	      	$this->session->set_userdata($session_data);
	      	echo TRUE;
		} else {
			$this->session->sess_destroy();
			echo FALSE;
		}
	}

	public function logout() {
		$this->session->sess_destroy();
		redirect(base_url() . 'login');
	}
}
/* End of file login.php */
/* Location: ./application/controllers/login.php */