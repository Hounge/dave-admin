<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>Admin Panel</title>
		<!-- load twitter bootstrap CSS from CDN -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css">
		<!-- load font awesome from CDN -->
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- load open sans font from Google Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
		<!-- load main CSS file using asset helper -->
		<?php echo css_asset('/admin.css'); ?>
		<!-- load jquery from CDN -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<!-- load twitter bootstrap javascript from CDN -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
	</head>
	<body>
		<nav class="navbar navbar-default navbar-fixed-top">
	  		<div class="container">
	    		<a class="navbar-brand" href="#">Admin Panel</a>
	  		</div>
		</nav>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="alert alert-success alert-dismissible" role="alert" id="message">
					  	<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					  	<span class="message"></span>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">Login</div>
				  		<div class="panel-body">
						    <form class="form-inline login-form">
								<div class="form-group">
									<label class="sr-only" for="username">Username</label>
									<input type="text" class="form-control" id="username" placeholder="Username">
								</div>
								<div class="form-group">
									<label class="sr-only" for="password">Password</label>
									<input type="password" class="form-control" id="password" placeholder="Password">
								</div>
								<button type="button" class="btn btn-info">Sign in</button>
							</form>
					  	</div>
					</div>
				</div>
			</div>
		</div>
		<script>
			$(document).ready(function() {
				$('.btn-info').click(function() {
					var username = $('#username').val();
					var password = $('#password').val();
					if (username == '') {
						$('#message .message').text('Please enter a valid username');
			            $('#message').removeClass('alert-success').addClass('alert-danger').fadeIn();
					}
					else if (password == '') {
						$('#message .message').text('Please enter your password');
			            $('#message').removeClass('alert-success').addClass('alert-danger').fadeIn();
					} else {
						$.ajax({
				            type: 'POST',
				            url: "<?php echo base_url(); ?>login/handle_login",
				            data: {username:username, password:password},
				            dataType: 'text',
				            success: function(response){
				              if (response) {
				              	window.location = 'admin';
				              } else {
				              	$('#message .message').text('Login Failed');
				              	$('#message').removeClass('alert-success').addClass('alert-danger').fadeIn();
				              }
				            }
				        });
					}
				});
				
				// trap the enter key press to trigger form submit
				$('form').bind('keypress', function(e){
					if ( e.keyCode == 13 ) {
						$( this ).find('.btn-info').click();
					}
				});
			});
		</script>
	</body>
</html>